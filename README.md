# Temperature and Humidity Data logger
THDL can be used in supply chain management to monitor the storage conditions 

## Goals: 
- To monitor air temperature and relative humidity levels at all times
- THDL is a system to detect any temperature anomalies and to alert on time so preventive maintenance can happen 

## Tasks: 
- Research 
- Code
- Document 
## Workflow: 
- **Contributors** takeup a specific task/issue and work on it and submit code/suggestions/data
- A contribution will be either approved by the ***Maintainer*** or ***Domain Lead***
- Person who contributes more becomes **Domain Lead**
## Resources:  
- [Getting Started with Shunya Interfaces](http://demos.iotiot.in/si/docs/diy/01-quickstart)
- [Project Planning Sheet](https://www.google.com/url?q=https://docs.google.com/spreadsheets/d/1ayq6BoayqRruMNtnFKO3ipDIQE3Sg529AXDOp_mnxx0/edit?usp%3Dsharing&sa=D&ust=1597855682599000&usg=AFQjCNFfvmIFfDtwsz-O4BIZAHLSN5Xx0g) 
- [Git Workflow](https://i.imgur.com/oodiCnB.png) 
- [Shunya Interfaces API reference](http://demos.iotiot.in/si/docs/reference/cloud/ref-aws)
