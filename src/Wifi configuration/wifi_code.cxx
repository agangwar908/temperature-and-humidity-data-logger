/** @file problem2.cxx
 *  @solution to wifi configuration in project
 *  
 *  Reads JSON file and configures  wifi, ethernet of the machine 
 *  
 *  @author Anurag singh 
 *  @bug  1.This program is specifically made to parse the test.json file and carry out the 
 *	    configurations accordingly. So please change the values alone and not the key name or 
 * 	    overall format of the file because it'll fail
 *
 * 	  3. NOTE: Program compiles with command "g++ wifi_code.cxx" works but it is not tested with 
 *	     cmake build commands
 */	     

/*
 *#####################################################################
 *  Instruction to test the program
 *  -------------------------------
 *  Change only the values in the provided test.json file for example: 
 *  change the values of status,ssid,password,etc for wifi;
 *
 *#####################################################################
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <cstdio>
#include <iostream>
#include <cstdlib>

/* --- Project Includes --- */
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filereadstream.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"

/* using standard namespace */
using namespace std; 
/* using rapidjson namespace */
using namespace rapidjson;

/* This will be used for removing obsolete code */
#define OBSOLETE 0

/* Declare a JSON document. JSON document parsed will be stored in this variable */
Document d;

/*
 *#####################################################################
 *  Process block
 *  -------------
 *  Solve all your problems here 
 *#####################################################################
 */

/** 
 *  @brief parse_file()
 *  
 *  parses the json file and stores it in document
 *
 *  @return nothing
 */

void parse_file(void)
{
	
	/* Open the example.json file in read mode */
        FILE* fp = fopen("test.json", "r"); 

        /* Declare read buffer */
        char readBuffer[65536];

        /* Declare stream for reading the example stream */
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));


        /* Parse example.json and store it in `d` */
        d.ParseStream(is);

        /* Close the example.json file*/
        fclose(fp);
	
}

/** 
 *  @brief set_timezone
 *  
 *  changes the system timezone to given timezone
 *
 *  @return nothing
 */

/** 
 *  @brief set_wifi()
 *  
 *  connects/disconnects wifi
 *
 *  @return nothing
 */

void set_wifi(void)
{
        /* Declare an object to store the  "wireless" "device" value */
	Value& wdevice = d["wireless"]["device"];
        /* Declare an object to store the  "wireless" "status" value */
	Value& wstatus = d["wireless"]["status"];

	/* string to store status value */
	string check = wstatus.GetString();

        /* check if the status value is on */
	if(check.compare("on") == 0){
                /* Declare an object to store the  "wireless" "ssid" value */
	        Value& wssid = d["wireless"]["ssid"];
                /* Declare an object to store the  "wireless" "password" value */
	        Value& wpass = d["wireless"]["password"];	
                /* String to store the ssid */
	        string ssid = wssid.GetString();
                /* String to store the password */
	        string pass =  wpass.GetString();

                /* send command to turn on the network manager */
	        system("nmcli n on");
                /* start the wpa_supplicatn.service */
	        system("sudo systemctl start wpa_supplicant.service");
                /* append the given ssid and password with the command*/
	        string str = "nmcli dev wifi connect "+ ssid +" "+"password "+ pass;
                /* convert the command string to const char type */       
	        const char *command = str.c_str();
                /* pass the command to the shell */
	        system(command);
	}

        /* check if the status value is off */
        else if(check.compare("off") == 0){
                /* turn off the network manager */
	        system("nmcli radio wifi off");
	}
	/* Print the message to the user */
	cout<<"------------------------------------------------------------------"<<endl;
	cout<<"                WIFI SETTINGS APPLIED                             "<<endl;
	cout<<"------------------------------------------------------------------"<<endl;

}

/** 
 *  @brief set_ethernet
 *  
 *  configures the ethernet connection
 *
 *  @return nothing
 */

void set_ethernet(void)
{
	/* Declare an object to store the  "ethernet" "device" value */
        Value& edevice1  = d["ethernet"][0]["device"];  
   
        /* Declare an object to store the  "ethernet" "status" value */
	Value& estatus1  = d["ethernet"][0]["status"];

        /* string to store the ethernet status value*/
	string check = estatus1.GetString();

        /* check if the status value is on */
	if(check.compare("on") == 0){
		
		#if OBSOLETE
			/*
		 	*#####################################################################
		 	*  Initially there were problems in modifying the interfaces file since  
			*  it was read-only. But this is highly unlikely, SO REMOVING THE CODE
			*  THAT WAS PREVIOUSLY USED FOR CHANGING THE FILE PERMISSIONS IN ORDER 
			*  ACCESS THE REQUIRED FILE
			*#####################################################################
			*/		
			system("sudo chmod 777 /etc/network/interfaces");
		#endif

		/* Declare an object to store the  "ethernet" "allocation" value */
		Value& ealloc1   = d["ethernet"][0]["allocation"]; 
	        /* Declare an object to store the  "ethernet" "ipaddress" value */
		Value& eipaddr1  = d["ethernet"][0]["ipAddress"];
       		/* Declare an object to store the  "ethernet" "subnetmask" value */
		Value& esubmask1 = d["ethernet"][0]["subnetMask"];
        	/* Declare an object to store the  "ethernet" "routeraddress" value */
		Value& eraddr1   = d["ethernet"][0]["routerAddress"];

                /* Print the message to the user */
		cout<<"------------------------------------------------------------------"<<endl;
	        cout<<"THIS OPERATION IS GOING TO MODIFY THE /etc/network/interfaces FILE"<<endl;
		cout<<"TAKING BACKUP OF THAT FILE CONTENTS TO interfaces_original"<<endl;
		cout<<"------------------------------------------------------------------"<<endl;
                /* create an empty file to store the settings */
	        system("touch interfaces_original");
	        /* copy the contents of the file to newly created file for backup */
                system("cp /etc/network/interfaces  interfaces_original"); 
	
                /* store the allocation value in a string */
	        string checkalloc = ealloc1.GetString();
                /* check if the allocation is static or dhcp */
	        if(checkalloc.compare("static") == 0){
                        /* static allocation
                         * combine the command and the specified parameters
                         * into a single string
                         */
		        string etherstr = "sudo echo \"# interfaces(5) file used by ifup(8) and ifdown(8)\nauto lo\niface lo inet loopback\nauto ";
			
		        etherstr = etherstr + edevice1.GetString();
		        etherstr = etherstr + "\niface ";
		        etherstr = etherstr + edevice1.GetString();
		        etherstr = etherstr + " inet static\naddress ";
		        etherstr = etherstr + eipaddr1.GetString() + "\nnetmask " + esubmask1.GetString() + "\ngateway " + eraddr1.GetString() + "\n\" > /etc/network/interfaces";  	
                        /* convert the command string to const char type */
		        const char *command = etherstr.c_str();
		        /* pass the command */
                        system(command);	

	        } //end of static allocation for device1


        	if(checkalloc.compare("dhcp") == 0){
                        /* dhcp allocation
                         * combine the command and the specified parameters
                         * into a single string
                         */
		        string etherstr = "sudo echo \"# interfaces(5) file used by ifup(8) and ifdown(8)\nauto lo\niface lo inet loopback\nauto ";
		        etherstr = etherstr + edevice1.GetString();
		        etherstr = etherstr + "\niface ";
		        etherstr = etherstr + edevice1.GetString();
		        etherstr = etherstr + " inet dhcp";
		        etherstr = etherstr + "\n\" > /etc/network/interfaces";  	
		        
                        /* convert the command string to const char type */
                        const char *command = etherstr.c_str();
		        /* pass the command */
                        system(command);
	
	        }//end of dhcp allocation for device1
		
		#if OBSOLETE
			/* revert /etc/network/interfaces to default permissions  */
			system("sudo chmod 755 /etc/network/interfaces");
		#endif

	}// end of device1 ethernet configuration
	
	else if(check.compare("off") == 0){
		
		string etherstr = "sudo ifconfig ";
		etherstr = etherstr + edevice1.GetString() + " down";
                /* convert the command string to const char type */
		const char *command = etherstr.c_str();
		/* pass the command */
                system(command);


	}//end of device1 disable

	/* Create and object and store values of the second 
         * device details like status,allocation,ipaddress,etc
         */

	Value& estatus2 = d["ethernet"][1]["status"];
	Value& edevice2 = d["ethernet"][1]["device"];

        /* string to store te status value */
	string check2 = estatus2.GetString();
        /* check if the status is on */	
	if(check2.compare("on") == 0){
		
		#if OBSOLETE
			/*
		 	*#####################################################################
		 	*  Initially there were problems in modifying the interfaces file since  
			*  it was read-only. But this is highly unlikely, SO REMOVING THE CODE
			*  THAT WAS PREVIOUSLY USED FOR CHANGING THE FILE PERMISSIONS IN ORDER 
			*  ACCESS THE REQUIRED FILE
			*#####################################################################
			*/		
			system("sudo chmod 777 /etc/network/interfaces");
		#endif
		
		Value& ealloc2  = d["ethernet"][1]["allocation"]; 
		Value& eipaddr2 = d["ethernet"][1]["ipAddress"];
		Value& esubmask2 = d["ethernet"][1]["subnetMask"];
		Value& eraddr2 = d["ethernet"][1]["routerAddress"];
	        /* string to store the allocation value */
                string checkalloc2 = ealloc2.GetString();
	        
                /* check if allocation is static */
                if(checkalloc2.compare("static") == 0){

                        /* static allocation
                         * combine the command and the specified parameters
                         * into a single string
                         */
		        string etherstr = "sudo echo \"# interfaces(5) file used by ifup(8) and ifdown(8)\nauto lo\niface lo inet loopback\nauto ";
		        etherstr = etherstr + edevice2.GetString();
		        etherstr = etherstr + "\niface ";
		        etherstr = etherstr + edevice2.GetString();
		        etherstr = etherstr + " inet static\naddress ";
		        etherstr = etherstr + eipaddr2.GetString() + "\nnetmask " + esubmask2.GetString() + "\ngateway " + eraddr2.GetString() + "\n\" >> /etc/network/interfaces";  	
		       
                        /* convert the command string to const char type */
                        const char *command = etherstr.c_str();
		        /* pass the command */
                        system(command);	

	        }//end of static allocation for device2

	        if(checkalloc2.compare("dhcp") == 0){
                        /* dhcp allocation
                         * combine the command and the specified parameters
                         * into a single string
                         */
		        string etherstr = "sudo echo \"auto ";
		        etherstr = etherstr + edevice2.GetString();
		        etherstr = etherstr + "\niface ";
		        etherstr = etherstr + edevice2.GetString();
		        etherstr = etherstr + " inet dhcp";
		        etherstr = etherstr + "\n\" >> /etc/network/interfaces";  	
		        
                        /* convert the command string to const char type */
                        const char *command = etherstr.c_str();
		        /* pass the command */
                        system(command);
	
	        }//end of dhcp allocation for device2
		
		#if OBSOLETE
			/* revert /etc/network/interfaces to default permissions */
			system("sudo chmod 755 /etc/network/interfaces");
		#endif

	}// end of device2 ethernet configuration

	else if(check2.compare("off") == 0){
		
		string etherstr = "sudo ifconfig ";
		etherstr = etherstr + edevice2.GetString() + " down";
                /* convert the command string to const char type */
		const char *command = etherstr.c_str();
		/* pass the command */
                system(command);

	}//end of device1 disable

	/* Print the message to the user */
	cout<<"------------------------------------------------------------------"<<endl;
	cout<<"               ETHERNET SETTINGS APPLIED                          "<<endl;
	cout<<"------------------------------------------------------------------"<<endl;
	
}//end of set_ethernet function



/** 
 *  @brief main function
 *  
 *  main entry point of the program
 *
 *  @return nothing 
 */

int main (void)
{
	/* Parse the json file */
        parse_file( );

        /* configure the wifi */
	set_wifi();

        /* configure the ethernet */
	set_ethernet();
        return 0;
}
